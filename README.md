# Veegil mobile banking app
## Name of Author
 OKPALA ANAYO CHIDIEBERE,
 okpalaanayo@gmail.com



## App description
This app is a demo app built for the fulfillment of the resquirement of Veegil mobile app frontend requirement. It is very basic as hence is not suitable for production. It was build with react-native using expo-cli. As fulfillment of the requirement a compiled apk is attached-veegil_android_m_app.apk.
This app allows user to signup, login, logout, fund their account,withdraw from their acount and see their transaction history from newest to oldest.


## How to run
steps
1. clone the repository
2. cd veegil-mobile-bankig-app
3. run npm install or yarn 
4. run npm run start or yarn start
follow the instruction to view

## Project status
I implemented this projoct based on the basic requirement of the veegil
 assessment task.
