import {
  Feather,
  MaterialCommunityIcons,
  MaterialIcons,
  FontAwesome,
  FontAwesome5,
} from '@expo/vector-icons'

export const colors = {
  gray: '#17223adb',
  input: '#c5d2f8,',
  backgrd:'#0a1e5e',
  bl2:'#2e4280'
}
export const griddata = [
  {
    title: 'Fund Account',
    link: 'Transfer',
    icon: () => (
      <MaterialCommunityIcons name="cash-plus" size={36} color="orange" />
    ),
  },
  {
    title: 'Withdraw',
    link: 'Withdraw',
    icon: () => (
      <MaterialCommunityIcons name="cash-minus" size={36} color="orange" />
    ),
  },
  {
    title: 'Transfer',
    link: 'Transfer2',
    icon: () => (
      <MaterialCommunityIcons name="transfer" size={36} color="orange" />
    ),
  },
  {
    title: 'Airtime',
    link: 'airtime',
    icon: () => <FontAwesome name="phone-square" size={36} color="orange" />,
  },
  {
    title: 'Pay bills',
    link: 'bills',
    icon: () => (
      <FontAwesome5 name="money-bill-wave" size={36} color="orange" />
    ),
  },
  {
    title: 'Gift Cards',
    link: 'homes',
    icon: () => (
      <MaterialIcons name="wallet-giftcard" size={36} color="orange" />
    ),
  },
  {
    title: 'History',
    link: 'History',
    icon: () => <FontAwesome name="history" size={36} color="orange" />,
  },
  // {
  //     title:'To bank',
  //     link:'homes'
  // },
]
// const styles = StyleSheet.create({
//     container: {
//       backgroundColor: colors.gray,
//       justifyContent: 'flex-end',
//       flex: 1,
//       padding: 20,
//       margin: 2,
//     },
//     wrapperView: {
//       flexDirection: 'row',
//       justifyContent: 'space-between',
//       alignItems: 'center',
//       borderRadius: 8,
//       padding: 2,
//       margin: 2,
//       borderWidth: 1,
//       borderColor: '#eee',
//       // backgroundColor:'skyblue',
//     },
//     submitButton: {
//       backgroundColor: 'green',
//       borderRadius: 8,
//       borderWidth: 1,
//       borderColor: 'orange',
//       justifyContent: 'center',
//       alignItems: 'center',
//       padding: 5,
//       borderColor: '#eee',
//       marginTop: 20,
//     },
//     input: {
//       borderWidth: 1,
//       borderRadius: 8,
//       borderColor: '#eee',
//       color: colors.input,
//       padding: 5,
//       flex: 1,
//     },
//     text: {
//       color: '#fff',
//       fontWeight: 'bold',
//       margin: 3,
//       minWidth: '30%',
//     },
//     link: {
//       color: 'blue',
//       alignItems: 'center',
//     },
//   })
