import axios from 'axios'
export const Axios = axios.create({
  baseURL: 'https://bankapi.veegil.com',
  headers: {
    Accept: '*/**',
    'Content-Type': 'application/json',
  },
})
export const getAccount = async (num) => {
  try {
    const { data } = await Axios.get('/auth/users')
    return data.data.filter((acc) => acc.phoneNumber === num)[0]
  } catch (e) {
    console.log(e.message, 'from get balance')
  }
}
export const getTransactions = async (num,setTXlist) => {
  try {
    const response = await Axios.get('/transactions')
    const result =response.data.data?.filter((acc) => acc.phoneNumber ===num).sort((a, b) => new Date(b.created)-new Date(a.created))
    console.log(result,'resSSULY')
setTXlist(result)
    return  result
  } catch (e) {
    console.log(e.message)
  }
}

export const formatCash =(amount)=>{
  const formattedNumber = new Intl.NumberFormat('en-US').format(amount);
  let decima =amount.toFixed(2).split('.')[1]
  return "₦"+ formattedNumber+"."+decima
}
export const formatDate =(datetime)=>{
  const options = {
    weekday: 'long',
    day: 'numeric',
    month: 'long',
    year: 'numeric',
  };
  const formattedDate = new Date(datetime).toLocaleDateString('en-US', options);
  return formattedDate;
}
export const formatTime =(date)=>{
const options = {
  hour: 'numeric',
  minute: '2-digit',
  hour12: true,
};
const formattedTime = new Date(date).toLocaleTimeString('en-US', options);
return formattedTime;
}
export const greet = (name = 'Dear Customer') => {
  const time = new Date(Date.now()).getHours();

  if (time < 12) {
    return "Good morning, " + name;
  } else if (time >= 12 && time <= 16) {
    return 'Good afternoon, ' + name;
  } else {
    return "Good evening, " + name;
  }
};
