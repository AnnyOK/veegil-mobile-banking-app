import { Stack } from 'expo-router'

const Layout = () =>{
    return (<Stack  screenOptions={{
        headerShown: false,
        tabBarVisible: false, // This will hide the tab bar by default
      }}/>)
}
export default Layout;