import 'react-native-gesture-handler'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import HomeScreen from '../screens/menuscreen'
import Login from '../screens/login'
import Register from '../screens/register'
import React from 'react'
import MenuScreen from '../screens/menu'
import Transfer from '../screens/addfund'
import Withdraw from '../screens/withdraw'
import Transactions from '../screens/transactions'
import Layout from './_layout'
import { useAuth } from '../screens/components/contextAuth'
import { AuthContextProvider } from '../screens/components/contextAuth'

const Index = () => {
  // const Stack = createStackNavigator()
  return (
    <AuthContextProvider>
      <Context />

    </AuthContextProvider>

  )
}
export default Index
const Context = ()=>{
  const {isLoggedIn}=useAuth()
  // console.log(isLoggedIn,"isLogin  router")
  if(isLoggedIn){
    return <MainStack/>
  }else{
    return<AuthStack/>
  }
}
const AuthStack = () => {
  const Stack = createStackNavigator()
  return (
    <Stack.Navigator  screenOptions={{
      headerShown: false,
      tabBarVisible: false, // This will hide the tab bar by default
    }}>
      {/* <Stack.Screen name="Dashboard" component={MenuScreen} /> */}
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="Login" component={Login} />

      <Stack.Screen name="Register" component={Register} />
      {/* <Stack.Screen name="Transfer" component={Transfer} /> */}
      {/* <Stack.Screen name="Withdraw" component={Withdraw} /> */}
      {/* <Stack.Screen name="History" component={Transactions} /> */}
    </Stack.Navigator>
  )
}
const MainStack = () => {
  const Stack = createStackNavigator()
  return (
    <Stack.Navigator  screenOptions={{
      headerShown: false,
      tabBarVisible: false, // This will hide the tab bar by default
    }}>
      {/* <Stack.Screen name="Login" component={Login} /> */}
      <Stack.Screen name="Dashboard" component={MenuScreen} />
      {/* <Stack.Screen name="Home" component={HomeScreen} /> */}
      {/* <Stack.Screen name="Register" component={Register} /> */}
      <Stack.Screen name="Transfer" component={Transfer} />
      <Stack.Screen name="Withdraw" component={Withdraw} />
      <Stack.Screen name="History" component={Transactions} />
    </Stack.Navigator>
  )
}
const $ViewStyle = {
  flex: 1,
  padding: 20,
  backgroundColor: '#FFDEAD',
}
