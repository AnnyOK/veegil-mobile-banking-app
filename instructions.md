Hi Okpala,

I reviewed your application for our Mobile Application Developer job and am impressed with your background.
I have decided to invite you to take a test for Mobile Application Developer at Veegil Technologies. The test aims to assess your design and development skills.

Assessment:

You are required to develop a simple banking application using Flutter (preferred), React Native, or other android/iOS technologies.

Consume the API documented below and available at https://bankapi.veegil.com/

API DOCUMENTATION

### URL + Requests Structures
- POST `/auth/signup`
```json
{
"phoneNumber": "xx123344444",
"password": "pass"
}
```

- POST `/auth/login`
```json
{
"phoneNumber": "xx123344444",
"password": "pass"
}
```

- POST `/accounts/transfer`
```json
{
"phoneNumber": "08100xxxxxx",
"amount":20000
}
```
- POST `/accounts/withdraw`
```json
{
"phoneNumber": "08100xxxxxx",
"amount":20000
}
```

- Get `/accounts/list`
- Get `/transactions`
- GET `/auth/users`
Decide on the required screens for the application using the following user stories.

1. Users should be able to sign up and login into their account. Users' phone numbers represent their account number.
2. Users should be able to deposit and send out money to others.
3. Users should be able to view a visualization that shows deposit and withdrawal history.
4. Users should be able to view the list of transactions.

Deadline: December 8, 2023 (23:59 PM) Nigerian Time

Mode of Submission:

1. Create a private repository on GitLab and
2. Add hr@veegil.com as a developer on the project containing your answer specifically.
3. Include your full name as it appears on Indeed in a README file in your GitLab project.
4. Describe your work and how to run your application in the README file
5. Compile your app as an apk file and include it in the repository. Add a link (if your file size is large) to your apk file.

Note: Do not add hr@veegil.com to a group on Gitlab.

Thank you for your participation.

Kind regards,

Samuel Adebesin,
Human Resources Officer,
Veegil Technologies Ltd.

Express
bankapi.veegil.com
Okpala Anayo sent the following messages at 1:20 PM
View Ok