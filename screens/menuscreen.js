import React from 'react'
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text
} from 'react-native'
import { colors } from '../helpers/airtifacts';
import Logo from './components/logo';

 const HomeScreen = ({navigation}) => {
  return (
    <View style={style.container}>
      <Logo/>
      <TouchableOpacity
        onPress={() => { 
             navigation.navigate('Register')
    }}
      >
        <Text style={style.registerButton}>REGISTER</Text>

      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => { 
          navigation.navigate('Login');
        }}
      >
<Text style={style.loginButton}>LOGIN</Text>
      </TouchableOpacity>
    </View>
  )
}
export default HomeScreen
const style = StyleSheet.create({
  container: {
    color: 'green',
    backgroundColor: colors.bl2,
    justifyContent: 'flex-end',
    flex: 1,
    padding: 20,
    margin: 2,
  },
  loginButton: {
    backgroundColor: 'tomato',
    width: '100%',
    marginTop: 5,
    borderWidth:1,
    borderRadius:8,
    borderColor:'orange',
    textAlign:'center',
    color:'#fff',
    fontWeight:'bold',
    paddingVertical:10,
    // flex: 1,
  },
  registerButton: {
    backgroundColor: 'green',
    width: '100%',
    marginTop: 5,
    borderWidth:1,
    borderRadius:8,
    textAlign:'center',
    color:'#fff',
    fontWeight:'bold',
    paddingVertical:10,
    borderColor:'orange',
  },
})
