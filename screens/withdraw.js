import React, { useEffect, useState } from 'react'
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Dimensions,
  Alert,
  ActivityIndicator
} from 'react-native'
import { Wrapper } from './wrapper'
import { useAuth } from './components/contextAuth'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { colors } from '../helpers/airtifacts'
import { AccountDetailCard } from './components/accountdetails'
import { Axios, getAccount } from '../helpers/helperfunctions'
import AsyncStorage from '@react-native-async-storage/async-storage'

function Withdraw({navigation}) {
  const { user, setUser } = useAuth()
  const [loading,setLoading] =useState(false)
  const [withdraw, setWithdraw] = useState({
    phoneNumber: user.phoneNumber,
    amount: 0,
  })
  const handleWithdraw = async () => {
    setLoading(true)
    try {
      const result = await Axios.post('/accounts/withdraw', withdraw)
      const acc = await getAccount(withdraw.phoneNumber)
      await AsyncStorage.setItem("user",JSON.stringify(acc))
setLoading(false)
setWithdraw({...withdraw,amount:0})
      Alert.alert(
        'Success',
        result.data.message,
        [
          { text: 'OK', onPress: () => navigation.navigate('Dashboard') },
        ],
        { cancelable: false },
      )
    } catch (e) {
      Alert.alert('Error', e.message, [
        { text: 'OK', onPress: () => console.log('error closed') },
      ])
    }
  }
  useEffect(() => {}, [withdraw])
  return (
    <Wrapper>
      <AccountDetailCard />
     {
     loading && <ActivityIndicator size='large' color={"orange"} style={{position:'absolute',top:'50%',zIndex:34}}/>

     }

      <View style={styles.grid}>
        <Text style={styles.text}>
          Phone Number: <Text>{user?.phoneNumber}</Text>
        </Text>
      </View>
      <View style={styles.grid}>
        <Text style={styles.text}>Enter Amount</Text>
        <TextInput
          style={styles.input}
          onChangeText={(text) =>
            setWithdraw({ ...withdraw, amount: Number(text) })
          }
          placeholder="0.00"
          placeholderTextColor="#aeAeAe" // Set your desired color here
          keyboardType="numeric"
          // placeholderTextColor={'white'}
          value={withdraw?.amount}
        />
      </View>
      <TouchableOpacity style={styles.button} onPress={handleWithdraw}>
        <Text style={styles.text}>Withdraw</Text>
      </TouchableOpacity>
    </Wrapper>
  )
}

export default Withdraw
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.gray,
    justifyContent: 'flex-start',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
    padding: 10,
    margin: 2,
  },
  grid: {
    border: 'none',
    borderRadius: 5,
    justifyContent: 'space-between',
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'center',
    width: Dimensions.get('window').width - 10,
    // height: 100,
    padding: 10,
    margin: 5,
    backgroundColor: '#0a1e5e',
  },
  button: {
    border: 'none',
    borderRadius: 5,
    width: Dimensions.get('window').width / 2,
    justifySelf: 'flex-end',
    // height: 100,
    padding: 10,
    margin: 5,
    backgroundColor: '#0a1e5e',
  },
  input: {
    backgroundColor: '#fff',
    width: '50%',
    height: 30,
    flex: 1,
    color: 'black',
    fontWeight: '900',
  },

  text: {
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 3,
    minWidth: '30%',
  },
  link: {
    color: 'blue',
    alignItems: 'center',
  },
})
