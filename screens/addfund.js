import React, { useState } from 'react'
import {
  View,
  Text,
  SafeAreaView,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  Alert,
  ActivityIndicator
} from 'react-native'
import { Wrapper } from './wrapper'
import { colors } from '../helpers/airtifacts'
import { TextInput } from 'react-native-gesture-handler'
import { Axios, getAccount } from '../helpers/helperfunctions'
import { AccountDetailCard } from './components/accountdetails'
import { useAuth } from './components/contextAuth'
import AsyncStorage from '@react-native-async-storage/async-storage'


function Addfund({navigation}) {
  const [loading,setLoading]= useState(false)
  const {setUser,user} = useAuth()
  const [data, setData] = useState({phoneNumber:user.phoneNumber})

  const handlefund = async () => {
    setLoading(true)
    try {
      console.log(data,"data addfund")
      const response = await Axios.post('accounts/transfer', data)
      const acc =await getAccount(data.phoneNumber)
      await AsyncStorage.setItem("user",JSON.stringify(acc))
      setLoading(false)
      Alert.alert(
        'Succcessful',
        response.data.message + '. Added ' + response.data.data.sent,
        [{ text: 'Ok', onPress: () => navigation.navigate('Dashboard') }],
        { cancelable: false },
      )
    } catch (e) {
      Alert.alert(
        'Error',
        e.message,
        [{ text: 'Ok', onPress: () => console.log('ok') }],
        { cancelable: false },
      )
    }
  }
  return (
    <Wrapper>
        <AccountDetailCard/>
        {
          loading&& <ActivityIndicator size='large' color={'orange'} style={{position:'absolute',top:'50%',zIndex:34}}/>
        }
      <View style={styles.grid}>
        <Text style={styles.text}>Add fund</Text>
      </View>
      <View style={styles.grid}>
        <Text style={styles.text}> Account(Phone Number)</Text>
        <TextInput
          style={styles.input}
          // onChangeText={(text) => setData({ ...data, phoneNumber: text })}
          // placeholder="e.g 0704555673"
          placeholderTextColor="#aeAeAe" // Set your desired color here
          keyboardType="numeric"
          value={user.phoneNumber}
          readOnly
        />
      </View>
      <View style={styles.grid}>
        <Text style={styles.text}>Enter Amount</Text>
        <TextInput
          style={styles.input}
          onChangeText={(text) => setData({ ...data, amount: Number(text) })}
          placeholder="0.00"
          placeholderTextColor="#aeAeAe" // Set your desired color here
          keyboardType="numeric"
          value={data?.amount}
        />
      </View>
      <TouchableOpacity style={styles.button} onPress={handlefund}>
        <Text style={styles.text}>Fund account</Text>
      </TouchableOpacity>
    </Wrapper>
  )
}

export default Addfund
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.gray,
    justifyContent: 'flex-start',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
    padding: 10,
    margin: 2,
  },
  grid: {
    border: 'none',
    borderRadius: 5,
    justifyContent: 'space-between',
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'center',
    width: Dimensions.get('window').width - 10,
    // height: 100,
    padding: 10,
    margin: 5,
    backgroundColor: '#0a1e5e',
  },
  button: {
    border: 'none',
    borderRadius: 5,
    width: Dimensions.get('window').width / 2,
    justifySelf: 'flex-end',
    // height: 100,
    padding: 10,
    margin: 5,
    backgroundColor: '#0a1e5e',
  },
  input: {
    backgroundColor: '#fff',
    width: '50%',
    height: 30,
    flex: 1,
    color: 'black',
    fontWeight: '900',
  },
  text: {
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 3,
    minWidth: '30%',
  },
  link: {
    color: 'blue',
    alignItems: 'center',
  },
})
