import React from 'react'
const image = require('../../helpers/images/logo.png')

import { ImageBackground,StyleSheet,View } from 'react-native'
const Logo = () => {
  return <ImageBackground source={image} resizeMode="cover" style={styles.logo} />

}
export default Logo
const styles = StyleSheet.create({
  logo: {
    position: 'absolute',
    top:100,
    marginLeft:100,
    // right:'50%',
    width: 150,
    height: 150,
  }, 
  view:{
    position:'absolute',
  }
})
