import { TouchableOpacity } from 'react-native-gesture-handler'
import { Text, StyleSheet, View } from 'react-native'
import { colors } from '../../helpers/airtifacts'
import { Feather } from '@expo/vector-icons'
import {
  formatCash,
  formatDate,
  formatTime,
} from '../../helpers/helperfunctions'
export const TransactionItem = ({ data }) => {
  return (
    <TouchableOpacity
      style={styles.item}
      //   onPress={()=>navigation.navigate(link)}
    >
      {/* <Text style={styles.text}>{data.type}</Text> */}
      {data.type === 'debit' ? (
        <Feather name="arrow-up" size={28} color="red" style={styles.eyeIcon} />
      ) : (
        <Feather
          name="arrow-down"
          size={28}
          color="green"
          style={styles.eyeIcon}
        />
      )}

      <Text style={styles.text}>{formatCash(data.amount)}</Text>
      <View>
        <Text style={styles.text}>{formatDate(data.created)}</Text>
        <Text style={styles.textsm}>{formatTime(data.created)}</Text>
      </View>
    </TouchableOpacity>
  )
}
const styles = StyleSheet.create({
  item: {
    backgroundColor: colors.backgrd,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    flex: 1,
    padding: 10,
    margin: 2,
    height: 100,
  },
  text: {
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 3,
    minWidth: '30%',
  },
  textsm: {
    color: '#999',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 3,
    minWidth: '30%',
  },
  link: {
    color: 'blue',
    alignItems: 'center',
  },
})
