import { TouchableOpacity } from 'react-native-gesture-handler'
import { Text, StyleSheet, Dimensions,View } from 'react-native'
import { colors } from '../../helpers/airtifacts'
import { useAuth } from './contextAuth'
import { useEffect } from 'react'
import { Feather } from '@expo/vector-icons'
import { MaterialIcons } from '@expo/vector-icons'

import { formatCash } from '../../helpers/helperfunctions'
const width = Dimensions.get('window').width
export const AccountDetailCard = ({ navigation }) => {
  const { user } = useAuth()
  useEffect(() => {}, [user])
  return (
    <TouchableOpacity style={styles.card}>
      <Text style={styles.text}>Account details</Text>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
        }}
      >
        <Text style={styles.text}>Account No:{'  '}</Text>
        <Text style={styles.text}>{user?.phoneNumber}</Text>
        <Feather name="user" size={40} color="#eee" style={styles.eyeIcon} />
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
        }}
      >
        <Text style={styles.text}>Account Balance:{'  '}</Text>
        <Text style={styles.text}>{formatCash(user?.balance)}</Text>
        <MaterialIcons name="account-balance" size={40} color="#eee" />
      </View>
    </TouchableOpacity>
  )
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.gray,
    justifyContent: 'center',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
    padding: 10,
    margin: 2,
  },
  card: {
    border: 'none',
    borderRadius: 5,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    width: width - 10,
    height: 150,
    padding: 10,
    marginTop: 20,
    marginHorizontal: 10,
    marginBottom: 10,
    backgroundColor: '#0a1e5e',
  },

  text: {
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 3,
    minWidth: '30%',
    flex: 1,
  },
  link: {
    color: 'blue',
    alignItems: 'center',
  },
  eyeIcon: {
    marginLeft: 20,
    padding: 10,
  },
})
