import AsyncStorage from '@react-native-async-storage/async-storage'
import React, { createContext, useContext, useEffect, useRef, useState } from 'react'
const AuthContext = createContext()
export const useAuth = () => {
  return useContext(AuthContext)
}
export const AuthContextProvider = ({ children }) => {
  const [user, setUser] = useState({phoneNumber:'',amount:''})
  const [isLoggedIn, updateIsLoggedIn] = useState(false);

  const _userdata = async () => {
      const userdetails= await AsyncStorage.getItem("user")
  setUser(JSON.parse(userdetails))
    try {
      const result = await AsyncStorage.getItem("token")
      // console.log(result)
      if (result !== null) {
        updateIsLoggedIn(true)
      }else{
        updateIsLoggedIn(false)
      }
      return result
    } catch (e) {
      console.log(error)
    }
  }
  const contextValue = {
    isLoggedIn,
    updateIsLoggedIn,
    user,
    setUser,
  }
  useEffect(() => {
    _userdata()
  }, [isLoggedIn,user])

  return (
    <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>
  )
}
