import { TouchableOpacity } from 'react-native-gesture-handler'
import { Text ,StyleSheet} from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useAuth } from './contextAuth'
import { Feather } from '@expo/vector-icons'
import { colors } from '../../helpers/airtifacts'
export const Logout = () => {
  const {updateIsLoggedIn} = useAuth()
  const handleLogout=async()=>{
    try{
      await AsyncStorage.removeItem('token')
      await AsyncStorage.removeItem('user')
      updateIsLoggedIn(false)
    }catch(e){
      console.log(e)
    }
  }
  return (
    <TouchableOpacity
      style={styles.grid}
      onPress={handleLogout}
    >
      <Text style={styles.text}>Logout</Text>
      <Feather
      name='log-out'
      size={40}
      color={colors.backgrd}/>
    </TouchableOpacity>
  )
}
const styles = StyleSheet.create({
   
    grid:{
        border:'none',
        borderRadius:5,
        justifyContent: 'center',
        alignContent: 'center',
        width : 80,
        height: 80,
        padding:10,
        backgroundColor:'orange'
    },
    text: {
      color: '#fff',
      fontWeight: 'bold',
      margin: 3,
      minWidth: '30%',
    },
    link: {
      color: 'blue',
      alignItems: 'center',
    },
  })
  