import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { Text, StyleSheet, ImageBackground, Dimensions } from 'react-native'
import { colors } from '../helpers/airtifacts'
import { SafeAreaView } from 'react-native-safe-area-context'
const image = require('../helpers/images/Veegil.png')
const windowHeight= Dimensions.get('window').height
const windowWidth= Dimensions.get('window').width

export const Wrapper = ({ children }) => {
  return (
    <SafeAreaView
    style={styles.container}
     >
      <ImageBackground source={image} resizeMode="cover" style={styles.image}>
        {children}
      </ImageBackground>
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.gray,
    justifyContent: 'flex-start',
    display: 'flex',
    padding:0,
    height:windowHeight,
    flexDirection: 'column',
    flexWrap: 'wrap',
    flex: 1,
  },scroll:{
    backgroundColor: colors.gray,
    minHeight: 800,
    display: 'flex',
    padding:0,
    flexDirection: 'column',
    flexWrap: 'wrap',
    flex: 1,
  },
  image: {
    flex: 1,
    justifyContent: 'center',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingTop:10,
    minHeight:windowHeight,
    width:windowWidth,
    margin: 'auto',
  },
  
})
