import React, { useEffect, useState, useMemo } from 'react'
import { View, StyleSheet, Dimensions, Text,ActivityIndicator } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import { Wrapper } from './wrapper'
import { getTransactions } from '../helpers/helperfunctions'
import { TransactionItem } from './components/transactionItem'
import { AccountDetailCard } from './components/accountdetails'
import { useAuth } from './components/contextAuth'
const placeholderdata = [
  {
    amount: 2000,
    phoneNumber: '07035182795',
    type: 'debit',
    created: '2015-10-25T12:34:56Z',
  },
  {
    amount: 4500,
    phoneNumber: '07035182795',
    type: 'credit',
    created: '2015-10-26T12:34:56Z',
  },
  {
    amount: 500,
    phoneNumber: '07035182795',
    type: 'credit',
    created: '2015-10-27T22:34:56Z',
  },
  {
    amount: 23000,
    phoneNumber: '07035182795',
    type: 'debit',
    created: '2015-10-28T12:34:56Z',
  },
  {
    amount: 20,
    phoneNumber: '07035182795',
    type: 'debit',
    created: '2015-10-29T12:34:56Z',
  },
  {
    amount: 1500,
    phoneNumber: '07035182795',
    type: 'credit',
    created: '2015-10-30T12:34:56Z',
  },
  {
    amount: 9000,
    phoneNumber: '07035182795',
    type: 'debit',
    created: '2015-10-31T09:34:56Z',
  },
  {
    amount: 2100,
    phoneNumber: '07035182795',
    type: 'credit',
    created: '2015-11-01T12:34:56Z',
  },
  {
    amount: 5000000,
    phoneNumber: '07035182795',
    type: 'credit',
    created: '2015-11-02T12:34:56Z',
  },
  {
    amount: 4350,
    phoneNumber: '07035182795',
    type: 'debit',
    created: '2015-11-03T03:34:56Z',
  },
]
function Transactions() {
  const [transaction, setTransaction] = useState([])
  const { user } = useAuth()
  useEffect(() => {
    getTransactions(user.phoneNumber, setTransaction)
  }, [])
  return (
    <Wrapper>
      <AccountDetailCard />
      {!transaction.length && 
      // <ActivityIndicator size='large' color='orange' style={{position:'absolute',top:'50%',right:"50%",zIndex:32}}/>
        <Text style={styles.text}>Currently no transaction to show or loading in progress</Text>
        }
      <View>
        
          <FlatList
            data={transaction}
            renderItem={({ item }) => <TransactionItem data={item} />}
            keyExtractor={(item) => item?.created}
            contentContainerStyle={styles.flat}
            style={styles.flatlist}
          />
      </View>
    </Wrapper>
  )
}

export default Transactions
const styles = StyleSheet.create({
  flat: {
    padding: 1,
    margin: 2,
    width: Dimensions.get('window').width,
  },
  flatlist: {
    flex: 1,
    maxHeight: Dimensions.get('window').height - 190,
  },
  text:{
    position:"absolute",top:'50%',
    color:'white',textAlign:'center',fontSize:18
  }
})
