import React, { useEffect, useState } from 'react'
import {
  View,
  Text,
  SafeAreaView,
  TextInput,
  StyleSheet,
  Alert,
} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { colors } from '../helpers/airtifacts'
import { Feather } from '@expo/vector-icons'
import { Axios, getAccount } from '../helpers/helperfunctions'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useAuth } from './components/contextAuth'
import Logo from './components/logo'

const Login = ({ navigation }) => {
  const [userData, setUserData] = useState({ phoneNumber: '', password: '' })
  const [eye, toggleEye] = useState(true)
  const {setUser,updateIsLoggedIn} =useAuth()

  const handleLogin = async () => {
    try {
      const { data } = await Axios.post('/auth/login', userData)
      await AsyncStorage.removeItem("token")
     await AsyncStorage.setItem("token",data.data.token)
     const account =userData.phoneNumber && await getAccount(userData.phoneNumber)
    if(account){
      await AsyncStorage.removeItem("user")
      await AsyncStorage.setItem("user",JSON.stringify(account))
updateIsLoggedIn(true)
    }
    } catch (e) {
      Alert.alert(
        'Error ',
        e.message,
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
          { text: 'Go Back', onPress: () => navigation.navigate('Login') },
        ],
        { cancelable: false },
      )
    }
  }
  // useEffect(() => {}, [userData])
  return (
    <SafeAreaView style={styles.container}>
      <Logo/>
      <TouchableOpacity style={styles.wrapperView}>
        <Text style={styles.text}>Phone Number: </Text>

        <TextInput
          onChangeText={(text) =>
            setUserData({ ...userData, phoneNumber: text })
          }
          placeholder="e.g ***3588999"
          style={styles.input}
          placeholderTextColor={'yellow'}
          value={userData.phoneNumber}
        />
      </TouchableOpacity>
      {/* password */}
      <TouchableOpacity style={styles.wrapperView}>
        <Text style={styles.text}>Password:</Text>
        <TextInput
          onChangeText={(text) => setUserData({ ...userData, password: text })}
          placeholder="e.g password"
          style={styles.input}
          secureTextEntry={eye}
          placeholderTextColor={'yellow'}
          value={userData.password}
        />
        {eye && (
          <Feather
            name="eye"
            size={28}
            color="#eee"
            style={styles.eyeIcon}
            onPress={() => toggleEye(!eye)}
          />
        )}
        {!eye && (
          <Feather
            name="eye-off"
            size={28}
            color="#eee"
            style={styles.eyeIcon}
            onPress={() => toggleEye(!eye)}
          />
        )}
      </TouchableOpacity>

      {/* submit */}
      <TouchableOpacity onPress={handleLogin} style={styles.submitButton}>
        <Text style={styles.text}>Login</Text>
      </TouchableOpacity>
      <Text style={styles.text}>
        Not Registered ?{' '}
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text style={styles.link}> Register</Text>
        </TouchableOpacity>
      </Text>
    </SafeAreaView>
  )
}

export default Login
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.bl2,
    justifyContent: 'flex-end',
    flex: 1,
    padding: 20,
    margin: 2,
  },
  wrapperView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 8,
    padding: 2,
    margin: 5,
    borderWidth: 1,
    borderColor: 'orange',
    backgroundColor:colors.backgrd,
  },
  submitButton: {
    backgroundColor: 'green',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: 'orange',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
    // borderColor: '#eee',
    marginTop: 20,
  },
  input: {
    backgroundColor:!colors.input,
    borderWidth: 1,
    borderRadius: 8,
    borderColor: 'orange',
    color: 'white',
    padding: 5,
    flex: 1,
  },
  text: {
    color: '#fff',
    fontWeight: 'bold',
    margin: 3,
    minWidth: '30%',
  },
  link: {
    color: 'blue',
    alignItems: 'center',
  },
  eyeIcon: {
    position: 'absolute',
    right: 5,
  },
})
