import React from 'react'
import {
  View,
  Text,
  ScrollViewComponent,
  StyleSheet,
  FlatList,
  ScrollView,
  Dimensions,
} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { SafeAreaView } from 'react-native-safe-area-context'
import { colors, griddata } from '../helpers/airtifacts'
import { Grid } from './grid'
import { Wrapper } from './wrapper'
import { AccountDetailCard } from './components/accountdetails'
import { Logout } from './components/logout'
import { greet } from '../helpers/helperfunctions'
import { useAuth } from './components/contextAuth'
export const MenuScreen = ({ navigation }) => {
  const {user} = useAuth()
  return (
    <Wrapper>
      <View>
        {/* <AccountDetailCard /> */}
        <TouchableOpacity
      style={styles.card}
    >
        <Text style={styles.greet}>{greet(user?.name)}</Text>

    </TouchableOpacity>

        <ScrollView
          contentContainerStyle={styles.container}
          style={styles.flatlist}
        >
          {griddata.map((grid, idx) => (
            <Grid
              title={grid.title}
              navigation={navigation}
              link={grid.link}
              icon={grid.icon}
              key={idx}
            />
          ))}
        </ScrollView>
        <View style={styles.gridContainer}>
          <TouchableOpacity style={styles.grid}>
            <Text style={styles.text}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.grid}>
            <Text style={styles.text}>Self Service</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.grid}>
            <Text style={styles.text}>news</Text>
          </TouchableOpacity>
          <Logout />
        </View>
      </View>
    </Wrapper>
  )
}
export default MenuScreen
const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
    height: Dimensions.get('screen').height - 300,
    padding: 10,
  },
  gridContainer: {
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    flexWrap: 'no-wrap',
    flex: 1,
    width:Dimensions.get('window').width,
    padding: 3,
    // margin: 'auto',
    position: 'absolute',
    bottom: 0,
  },
  card: {
    border: 'none',
    borderRadius: 5,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    width: Dimensions.get('window').width - 10,
    height: 150,
    padding: 10,
    marginTop: 20,
    marginHorizontal: 10,
    marginBottom: 10,
    backgroundColor: '#0a1e5e',
  },

  grid: {
    border: 'none',
    borderRadius: 5,
    width: 80,
    height: 80,
    padding: 10,
    // margin: 5,
    backgroundColor: colors.backgrd,
  },
  flatlist: {
    flex: 1,
    maxHeight: Dimensions.get('window').height - 190,
    margin:'auto',
    // height:100
  },

  text: {
    color: '#fff',
    fontWeight: 'bold',
    margin: 3,
    minWidth: '30%',
  },
  link: {
    color: 'blue',
    alignItems: 'center',
  },
  greet:{
    fontSize:36,
    color:'white',
    textAlign:'left'
  }
})
