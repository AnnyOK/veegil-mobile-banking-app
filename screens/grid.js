import { TouchableOpacity } from 'react-native-gesture-handler'
import { Text ,StyleSheet} from 'react-native'
import { colors } from '../helpers/airtifacts'
export const Grid = ({navigation,title,icon,link,}) => {
  return (
    <TouchableOpacity
      style={styles.grid}
      onPress={()=>navigation.navigate(link)}
    >
      {icon&& icon()}
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  )
}
const styles = StyleSheet.create({
    container: {
      backgroundColor: colors.gray,
      justifyContent: 'center',
    display:"flex",
    flexDirection:'row',
    flexWrap: 'wrap',
      flex: 1,
      padding: 10,
      margin: 2,
    //   width: 300,
    //   height:100
    },
    grid:{
        border:'none',
        borderRadius:5,
        display:"flex",
        flexDirection:"column",
        justifyContent:"space-between",
        width : 100,
        height: 100,
        padding:10,
        margin:1,
        backgroundColor:'#0a1e5e'
    },
    
   
  
    text: {
      color: '#fff',
      fontWeight: 'bold',
      margin: 3,
      minWidth: '30%',
    },
    link: {
      color: 'blue',
      alignItems: 'center',
    },
  })
  